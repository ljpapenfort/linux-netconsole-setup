#!/bin/bash
set -e

# get parameters
SSH_PUBLIC_KEY_FILE=${1:-"$HOME/.ssh/authorized_keys"}
TARGET_ISO=${2:-"`pwd`/ubuntu-18.04-netboot-amd64-unattended.iso"}

# check if ssh key exists
if [ ! -f "$SSH_PUBLIC_KEY_FILE" ];
then
    echo "Error: public SSH key $SSH_PUBLIC_KEY_FILE not found!"
    exit 1
fi

# get directories
CURRENT_DIR="`pwd`"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TMP_DOWNLOAD_DIR="`mktemp -d`"
TMP_DISC_DIR="`mktemp -d`"
TMP_INITRD_DIR="`mktemp -d`"

# Password file
passwdfile=${3:-"$CURRENT_DIR/passwd"}
ncpasswdfile=${3:-"$CURRENT_DIR/ncpasswd"}
usernamefile=${3:-"$CURRENT_DIR/username"}
hostnamefile=${3:-"$CURRENT_DIR/hostname"}

# download and extract netboot iso
SOURCE_ISO_URL="http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/mini.iso"
cd "$TMP_DOWNLOAD_DIR"
wget -4 "$SOURCE_ISO_URL" -O "./netboot.iso"
7z x "./netboot.iso" "-o$TMP_DISC_DIR"

# patch boot menu
cd "$TMP_DISC_DIR"
patch -p1 -i "$SCRIPT_DIR/custom/boot-menu.patch"

# prepare assets
cd "$TMP_INITRD_DIR"
mkdir "./custom"
cp "$SCRIPT_DIR/custom/preseed.cfg" "./preseed.cfg"
cp "$SCRIPT_DIR/custom/dropbear_config" "./custom/dropbear_config"
cp "$SSH_PUBLIC_KEY_FILE" "./custom/userkey.pub"

# Read files and if they don't exist, ask for input.
if [[ -f "$passwdfile" && -s "$passwdfile" ]]; then
	pwhash=$(<$passwdfile)
else
	read -sp " please enter your preferred password: " password
	printf "\n"
	read -sp " confirm your preferred password: " password2
	printf "\n"
	# generate the password hash
	pwhash=$(echo $password | mkpasswd -s -m sha-512)

	# check if the passwords match to prevent headaches
	if [[ "$password" != "$password2" ]]; then
	    echo " your passwords do not match; please restart the script and try again"
	    echo
	    exit
	fi
fi

if [[ -f "$ncpasswdfile" && -s "$ncpasswdfile" ]]; then
	ncpassword=$(<$ncpasswdfile)
else
	read -sp " please enter your preferred network console password: " ncpassword
	printf "\n"
	read -sp " confirm your preferred network console password: " ncpassword2
	printf "\n"

	# check if the passwords match to prevent headaches
	if [[ "$ncpassword" != "$ncpassword2" ]]; then
	    echo " your passwords do not match; please restart the script and try again"
	    echo
	    exit
	fi
fi

if [[ -f "$usernamefile" && -s "$usernamefile" ]]; then
	username=$(<$usernamefile)
else
	read -p " please enter your preferred username: " username
	printf "\n"
fi

if [[ -f "$hostnamefile" && -s "$hostnamefile" ]]; then
	hostname=$(<$hostnamefile)
else
	read -p " please enter your preferred hostname: " hostname
	printf "\n"
fi

echo;

# update the seed file to reflect the users' choices
# the normal separator for sed is /, but both the password and the timezone may contain it
# so instead, I am using @
sed -i "s@{{username}}@$username@g" "./preseed.cfg"
sed -i "s@{{pwhash}}@$pwhash@g" "./preseed.cfg"
sed -i "s@{{hostname}}@$hostname@g" "./preseed.cfg"
sed -i "s@{{ncpassword}}@$ncpassword@g" "./preseed.cfg"

# append assets to initrd image
cd "$TMP_INITRD_DIR"
cat "$TMP_DISC_DIR/initrd.gz" | gzip -d > "./initrd"
echo "./preseed.cfg" | fakeroot cpio -o -H newc -A -F "./initrd"
find "./custom" | fakeroot cpio -o -H newc -A -F "./initrd"
cat "./initrd" | gzip -9c > "$TMP_DISC_DIR/initrd.gz"

# build iso
cd "$TMP_DISC_DIR"
rm -r '[BOOT]'
mkisofs -r -V "ubuntu 18.04 netboot unattended" -cache-inodes -J -l -b isolinux.bin -c boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -input-charset utf-8 -o "$TARGET_ISO" ./

# go back to initial directory
cd "$CURRENT_DIR"

# delete all temporary directories
rm -r "$TMP_DOWNLOAD_DIR"
rm -r "$TMP_DISC_DIR"
rm -r "$TMP_INITRD_DIR"

# done
echo "Next step: add grub menu item (see grub example 40_custom file) and connect to the installer network console"
