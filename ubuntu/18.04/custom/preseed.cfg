### Preseed for Ubuntu 18.04
# Derived from: https://help.ubuntu.com/lts/installation-guide/example-preseed.txt

### Compatibility
# Tested with the netboot image:
# http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/current/images/netboot/mini.iso
# Might work with the regular image (not tested):
# http://releases.ubuntu.com/18.04/ubuntu-18.04-beta2-live-server-amd64.iso

### Authentication
# root account is disabled as well as ssh password authentication.
# Public keys are copied automatically during the installation, see below.
# Only the new user created is allowed to login through ssh.


### Unattended Installation
d-i auto-install/enable boolean true
d-i debconf/priority select critical
d-i debian-installer/splash boolean false

### Localization
d-i debian-installer/locale string en_US.UTF-8
d-i localechooser/supported-locales multiselect en_US.UTF-8
d-i console-setup/ask_detect boolean false
d-i keyboard-configuration/xkb-keymap select de
d-i keyboard-configuration/layoutcode string de
d-i keyboard-configuration/variantcode string

### Network configuration
d-i netcfg/choose_interface select auto
d-i netcfg/dhcp_timeout string 5
d-i netcfg/get_hostname string {{hostname}}
d-i netcfg/get_domain string {{hostname}}
d-i hw-detect/load_firmware boolean true

# If you have a slow dhcp server and the installer times out waiting for
# it, this might be useful.
d-i netcfg/dhcp_timeout string 10
d-i netcfg/dhcpv6_timeout string 5

### Mirror settings
d-i mirror/country string manual
d-i mirror/http/hostname string de.archive.ubuntu.com
d-i mirror/http/directory string /ubuntu

### Network console
# Use the following settings if you wish to make use of the network-console
# component for remote installation over SSH. This only makes sense if you
# intend to perform the remainder of the installation manually.
d-i anna/choose_modules string network-console
#d-i network-console/authorized_keys_url string http://10.0.0.1/openssh-key
d-i network-console/password password {{ncpassword}}
d-i network-console/password-again password {{ncpassword}}

### Account setup
d-i passwd/root-login                                       boolean     false
d-i passwd/make-user                                        boolean     true
d-i passwd/user-fullname                                    string      {{username}}
d-i passwd/username                                         string      {{username}}
d-i passwd/user-password-crypted                            password    {{pwhash}}
#d-i passwd/user-uid                                         string
d-i user-setup/allow-password-weak                          boolean     true
#d-i passwd/user-default-groups                              string      adm cdrom dialout lpadmin plugdev sambashare sudo
d-i user-setup/encrypt-home                                 boolean     false

### Clock and time zone setup
d-i clock-setup/utc boolean true
d-i time/zone string Europe/Berlin
d-i clock-setup/ntp boolean true
d-i clock-setup/ntp-server string ntp.ubuntu.com

### Partitioning
#d-i preseed/early_command string umount /media || true
#d-i partman-auto/disk string /dev/sda
#d-i partman-auto/method string lvm
#d-i partman-lvm/device_remove_lvm boolean true
#d-i partman-lvm/device_remove_lvm_span boolean true
#d-i partman-auto/purge_lvm_from_device  boolean true
#d-i partman-auto-lvm/new_vg_name string system
#d-i partman-lvm/confirm_nooverwrite boolean true
#d-i partman-auto/purge_lvm_from_device boolean true
#d-i partman-auto-lvm/guided_size    string  max
#d-i partman-lvm/confirm boolean true
#d-i partman/confirm_write_new_label boolean true

### Disk layout#

#d-i partman-auto/expert_recipe string         \
#  boot-root ::                                \
#    512 512 512 ext4                          \
#      $primary{ }                             \
#      $bootable{ }                            \
#      method{ format } format{ }              \
#      use_filesystem{ } filesystem{ ext4 }    \
#      mountpoint{ /boot }                     \
#    .                                         \
#    1024 50 1048576 ext4            \
#      $lvmok{ }                               \
#      method{ format } format{ }              \
#      use_filesystem{ } filesystem{ ext4 }    \
#      mountpoint{ / }                         \
#      lv_name{ root }                         \
#    .                                         \

#d-i partman-auto/choose_recipe select boot-root
#d-i partman-lvm/confirm boolean true
#d-i partman/confirm_write_new_label boolean true
#d-i partman/confirm_nooverwrite boolean true
#d-i partman/choose_partition select finish
#d-i partman/confirm boolean true
#d-i partman-basicfilesystems/no_swap boolean false
#d-i partman-swapfile/size string 0

### Base system installation
d-i base-installer/install-recommends boolean true
d-i base-installer/kernel/image string linux-generic

### Apt setup
d-i apt-setup/restricted boolean true
d-i apt-setup/universe boolean true
d-i apt-setup/multiverse boolean true
d-i apt-setup/backports boolean true
d-i apt-setup/use_mirror boolean true
d-i apt-setup/services-select multiselect security
d-i apt-setup/security_host string de.archive.ubuntu.com
d-i apt-setup/security_path string /ubuntu

### Package selection
d-i tasksel/first multiselect none
d-i pkgsel/include string openssh-server nano htop screen git curl dropbear-initramfs build-essential software-properties-common
d-i pkgsel/upgrade select full-upgrade
d-i pkgsel/update-policy select unattended-upgrades
d-i grub-installer/only_debian boolean true
d-i grub-installer/with_other_os boolean true
#d-i grub-installer/bootdev string /dev/sda
d-i grub-installer/bootdev string default

### Finishing up the installation
d-i preseed/late_command string \
 cp -r /custom /target/custom; \

# in-target sh -c 'usermod -p "!" root'; \
# in-target sh -c 'mkdir -p --mode=0700 /root/.ssh && cat /custom/userkey.pub > /root/.ssh/authorized_keys && chmod 0600 /root/.ssh/authorized_keys';  \

 in-target sh -c 'mkdir -p --mode=0700 /etc/dropbear-initramfs && cat /custom/userkey.pub > /etc/dropbear-initramfs/authorized_keys && chmod 0600 /etc/dropbear-initramfs/authorized_keys && cat /custom/dropbear_config > /etc/dropbear-initramfs/config && chmod 0600 /etc/dropbear-initramfs/config';  \
# in-target sh -c 'update-initramfs -u'; \

 in-target sh -c 'mkdir -p --mode=0700 /home/{{username}}/.ssh && cat /custom/userkey.pub > /home/{{username}}/.ssh/authorized_keys && chmod 0600 /home/{{username}}/.ssh/authorized_keys';  \
 in-target sh -c 'chown -R {{username}}:{{username}} /home/{{username}}/.ssh'; \

# in-target sh -c 'sed -i "s/^#PermitRootLogin.*\$/PermitRootLogin prohibit-password/g" /etc/ssh/sshd_config'; \

 in-target sh -c 'sed -i "s/^#PermitRootLogin.*\$/PermitRootLogin no/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'sed -i "s/^#StrictModes.*\$/StrictModes yes/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'sed -i "s/^#MaxAuthTries.*\$/MaxAuthTries 1/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'sed -i "s/^#PubkeyAuthentication.*\$/PubkeyAuthentication yes/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'sed -i "s/^#AuthorizedKeysFile.*\$/AuthorizedKeysFile    .ssh/authorized_keys/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'sed -i "s/^#IgnoreRhosts.*\$/IgnoreRhosts yes/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'sed -i "s/^#PermitEmptyPasswords.*\$/PermitEmptyPasswords no/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'sed -i "s/^#PasswordAuthentication.*\$/PasswordAuthentication no/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'sed -i "s/^#AllowAgentForwarding.*\$/AllowAgentForwarding yes/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'sed -i "s/^X11Forwarding.*\$/X11Forwarding no/g" /etc/ssh/sshd_config'; \
 in-target sh -c 'echo "AllowUsers username" >> /etc/ssh/sshd_config'; \

# in-target sh -c 'rm -f /etc/ssh/ssh_host_*_key* && mkdir -p /usr/lib/systemd/system && cp /custom/ssh-host-keygen.service /usr/lib/systemd/system/ssh-host-keygen.service && systemctl enable ssh-host-keygen.service'; \
# in-target sh -c 'echo "IPv4: \\\4" >> /etc/issue && echo "IPv6: \\\6" >> /etc/issue && echo "" >> /etc/issue'; \
# in-target sh -c 'cp /custom/init-user-home.sh /home/{{username}}/init-user-home.sh'; \
# in-target sh -c 'cp /custom/init-host.sh /root/init-host.sh; echo "bash ~/init-host.sh" >> /root/.profile'; \

 in-target sh -c 'eject || true'; \
 rm -r /target/custom;
d-i debian-installer/splash boolean false
d-i cdrom-detect/eject boolean true

### Shutdown machine
#d-i finish-install/reboot_in_progress note
#d-i debian-installer/exit/halt boolean false
#d-i debian-installer/exit/poweroff boolean false
